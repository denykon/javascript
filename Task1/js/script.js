var screenEl = document.getElementById('screen');

var newNumber = true;
var result = 0;
var display = '';
var operation = '';

var onNum = function (num) {
    if (newNumber) {
        display = num.toString();
        newNumber = false;
    } else {
        if (display == '0') {
            display = num.toString();
        } else if (display.length < 6) {
            display += num.toString();
        }
    }
    screenEl.innerHTML = display;
};

var onAdd = function () {
    calculate(operation);
    operation = '+';
};

var onSub = function () {
    calculate(operation);
    operation = '-';
};

var onDiv = function () {
    calculate(operation);
    operation = '/';
};

var onMul = function () {
    calculate(operation);
    operation = '*';
};

var onCompute = function () {
    calculate(operation);
    operation = '=';
};

var onReset = function () {
    result = 0;
    display = '';
    operation = '';
    newNumber = true;
    screenEl.innerHTML = '0';
    screenEl.style.fontSize = '5em';
};

function calculate(operation) {

    display = screenEl.innerHTML;
    if (newNumber && operation != "=") {
        show(result);
    } else {
        newNumber = true;
        if ('+' === operation) {
            result += parseFloat(display);
        } else if ('-' === operation) {
            result -= parseFloat(display);
        } else if ('/' === operation) {
            result /= parseFloat(display);
        } else if ('*' === operation) {
            result *= parseFloat(display);
        } else {
            result = parseFloat(display);
        }
        show(result);
    }
}

function show(result){

    if (result > 999999){
        result = result.toExponential(2);
        screenEl.style.fontSize = '3em';
    } else if (result.toString().length > 6){
        result = result.toPrecision(4);
        screenEl.style.fontSize = '3em';
    } else {
        screenEl.style.fontSize = '5em';
    }
    screenEl.innerHTML = result.toString();
}