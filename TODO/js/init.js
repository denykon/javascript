var collection = [];
var flag = 'all';
var listOfTodoes = document.getElementById('todoes-list');

var allFilter = document.getElementById('all-filter');
var todoFilter = document.getElementById('todo-filter');
var doneFilter = document.getElementById('done-filter');

var textField = document.getElementById('todoTittle');

var upSort = document.getElementById('up');
var downSort = document.getElementById('down');

allFilter.addEventListener('click', function (e) {
    e.preventDefault();
    flag = 'all';
    show();
});
todoFilter.addEventListener('click', function (e) {
    e.preventDefault();
    flag = 'todo';
    show();
});
doneFilter.addEventListener('click', function (e) {
    e.preventDefault();
    flag = 'done';
    show();
});

textField.addEventListener('keypress', function (e) {
    var key = e.which || e.keyCode;
    if (key === 13) {
        addTodo();
    }
});

upSort.addEventListener('click', function (e) {
    e.preventDefault();
    collection.sort(normalComparator);
    show();
});
downSort.addEventListener('click', function (e) {
    e.preventDefault();
    collection.sort(invertedComparator);
    show();
});

//TaskObject
var Todo = function (tittle) {
    this.tittle = tittle;
    this.completed = false;
};



