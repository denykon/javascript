var changeTodoStatus = function () {
    if (collection[this.parentNode.getAttribute('id')].completed == false) {
        this.parentNode.className = 'done';
        collection[this.parentNode.getAttribute('id')].completed = true;
    } else {
        this.parentNode.className = 'todo';
        collection[this.parentNode.getAttribute('id')].completed = false;
    }
    show();
};

var deleteTodo = function () {
    collection.splice(this.parentNode.getAttribute('id'), 1);
    show();
    if (collection.length == 0) {
        document.getElementById('list-filter').className = 'hide';
    }
    if (collection.length < 2) {
        document.getElementById('sort').className = 'hide';
    }
};

var showDelImg = function () {
    var elements = this.getElementsByTagName('img');
    elements[0].className = 'show';
};

var hideDelImg = function () {
    var elements = this.getElementsByTagName('img');
    elements[0].className = 'hide';
};

var htmlClear = function () {
    while (listOfTodoes.childElementCount > 0) {
        listOfTodoes.removeChild(listOfTodoes.firstElementChild);
    }
};

//adds task to collection
var addTodo = function () {
    var tittle = document.getElementById('todoTittle').value;
    if (tittle != '') {
        document.getElementById('todoTittle').value = '';
        var todo = new Todo(tittle);
        collection.push(todo);
        show();
        document.getElementById('list-filter').className = 'show';
        if (collection.length > 1) {
            document.getElementById('sort').className = 'show';
        }
    }
};

var listCreator = function (i) {
    var li = document.createElement('li');
    var label = document.createElement('label');
    var image = document.createElement('img');
    image.setAttribute('src', 'images/del.png');
    image.setAttribute('class', 'hide');
    image.addEventListener('click', deleteTodo);
    li.setAttribute('id', i.toString());
    if (collection[i].completed === true) {
        li.setAttribute('class', 'done');
    } else {
        li.setAttribute('class', 'todo');
    }
    label.innerHTML = collection[i].tittle;
    li.appendChild(label);
    li.appendChild(image);
    listOfTodoes.appendChild(li);
    label.addEventListener('click', changeTodoStatus);
    li.addEventListener('mouseover', showDelImg);
    li.addEventListener('mouseleave', hideDelImg);
};

var show = function () {

    htmlClear();

    switch (flag) {
        case 'all':
            for (var i = 0; i < collection.length; i++) {
                listCreator(i);
            }
            break;
        case 'todo':
            for (var i = 0; i < collection.length; i++) {
                if (collection[i].completed === false) {
                    listCreator(i);
                }
            }
            break;
        case 'done':
            for (var i = 0; i < collection.length; i++) {
                if (collection[i].completed === true) {
                    listCreator(i);
                }
            }
            break;
    }
};

var normalComparator = function (a, b) {
    if (a.tittle > b.tittle) {
        return 1;
    }
    if (a.tittle < b.tittle) {
        return -1;
    }
    return 0;
};
var invertedComparator = function (a, b) {
    if (a.tittle > b.tittle) {
        return -1;
    }
    if (a.tittle < b.tittle) {
        return 1;
    }
    return 0;
};